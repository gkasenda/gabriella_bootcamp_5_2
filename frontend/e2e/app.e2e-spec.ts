import { EventPage } from './app.po';

describe('event App', () => {
  let page: EventPage;

  beforeEach(() => {
    page = new EventPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
